const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");

// retrieve cart
module.exports.getCart = async (userId) => {
	try {
		const cart = await Cart.findOne({ userId });
		if (cart && cart.products.length > 0) {
			return cart;
		} else {
			return 'No existing cart';
		}
	} catch (error) {
		return false;
	}	
};


// add to cart
module.exports.addToCart = async (userId, { productId, quantity }) => {
	
	try {
		const cart = await Cart.findOne({ userId });
		const product = await Product.findOne({ _id: productId });

		if (!product) {
			return Promise.resolve({ message: 'Product not found!' });
		}

		const price = product.price;
		const name = product.name;
		const subtotal = quantity * price;
		// If cart already exists for user,
		if (cart) {
			const productIndex = cart.products.findIndex((product) => product.productId == productId);
			//check if product exists or not

			if (productIndex > -1) {
				let item = cart.products[productIndex];
				item.quantity += quantity;
				item.subtotal += subtotal;
				
				cart.amount = cart.products.reduce((acc, curr) => {
					return acc + curr.subtotal;
				},0);

				cart.products[productIndex] = item;
				await cart.save();
				return cart;
			} else {
				cart.products.push({ productId, name, quantity, price, subtotal});
				cart.amount = cart.products.reduce((acc, curr) => {
					return acc + curr.subtotal;
				},0)

				await cart.save();
				return cart;
			}
		} else {
			// no cart exist, create one
			const newCart = await Cart.create({
				userId,				
				products: [{ productId, name, quantity, price, subtotal }],
				amount: subtotal,
			});
			return newCart.save();
		} 
	} catch (error) {
		console.log(error);
		return 'Something went wrong';
	}
};


// Remove product in cart
module.exports.removeFromCart = async (userId, reqParams) => {
	try {
		let cart = await Cart.findOne({ userId });

		const productIndex = cart.products.findIndex((product) => product.productId == reqParams.productId);

		if (productIndex > -1) {
			let product = cart.products[productIndex];
			cart.amount -= product.quantity * product.price;
			if (cart.amount < 0) {
				cart.amount = 0
			}
			cart.products.splice(productIndex, 1);
			cart.amount = cart.products.reduce((acc, curr) => {
				return acc + curr.quantity * curr.price;
			},0) 
			cart = await cart.save();

			return cart;
		} else {
			return Promise.resolve({ value: 'Product not found!' });
		}
	} catch (error) {
		console.log(error);
		return 'Bad request';
	}
};





