const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exist
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false
		};
	});
};


// UserRegistration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,		
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// User Authentication / Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				// If password do not match
			} else {
				return false;
			}
		};
	});
};


// Modify USER role | set/unset as admin (ADMIN only)
module.exports.setAsAdmin = (data, reqParams) => {
    return User.findById(reqParams.userId).then(user => {
        if (!user) {
            return 'User not found';
        }
        if (data.isAdmin) {
            const updateAdminField = {
                isAdmin: !user.isAdmin
            };

            return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((updatedUser, error) => {
                if (error) {
                    return 'Something went wrong';
                } else {
                    const message = updatedUser.isAdmin ? 'User has been unset as an admin' : 'User has been set as an admin';
                    return message;
                }
            });
        } else {
            return 'User must be ADMIN to access this!';
        }
    });
};

// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};