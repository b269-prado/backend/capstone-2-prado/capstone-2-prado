const Product = require("../models/Product");

// Create new product (ADMIN only)
module.exports.addProduct = (data) => {	
	if (data.isAdmin) {		
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});		
		return newProduct.save().then((product, error) => {			
			if (error) {
				return false;			
			} else {
				return true;
			};
		});	
	};	
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return{value};
	});
};


// Retrieve ALL products (ADMIN only)
module.exports.getAllProducts = (data) => {
	if (data.isAdmin) {
		return Product.find({}).then(result => {
			return result;
		});
	}
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return{value};
	});	
};


// Retrieve ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieve SPECIFIC product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update a product's information (ADMIN only)
module.exports.updateProduct = (data, reqParams) => {
	if (data.isAdmin) {
		let updatedProduct = {
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		};

		// findByIdAndUpdate (document ID, updatesToBeApplied)
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return{value};
	});		
};


// Archive/unarchive a product (ADMIN only)
module.exports.archiveProduct = (data, reqParams) => {
	if (data.isAdmin) {
		return Product.findById(reqParams.productId).then((product, error) => {
			if (error) {
				return false;
			} else {
				product.isActive = !product.isActive;
				return product.save().then((updatedProduct, error) => {
					if (error) {
						return false;
					} else {
						return true;
					}
				});
			}
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return { value };
	});	
};

