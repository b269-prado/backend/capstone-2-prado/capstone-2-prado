const Order = require("../models/Order");
const Cart = require("../models/Cart");
const User = require("../models/User");

// Retrieve order
module.exports.getOrder = async (userId) => {
	try {
		const order = await Order.find({ userId: userId }).sort({ date: -1 });
		if (order.length === 0) {
			return Promise.resolve({ message: 'No orders found!' });;
		} else {
			return order;
		}
	} catch (error) {
		return false;
	}
};

// User checkout
module.exports.checkOut = async (userId, isAdmin) => {
	try {
		const user = await User.findById(userId);
		if (!user) {
			return Promise.resolve({ message: 'User not found!' });
		}
		if (isAdmin) {
			return Promise.resolve({ message: 'Admin users cannot checkout orders!' });
		}
		const cart = await Cart.findOne({ userId: userId });
		if (!cart) {
			return Promise.resolve({ message: 'Cart not found!' });
		}
		const order = new Order({
			userId: userId,
			products: cart.products,
			amount: cart.amount
		});
		await order.save();
		cart.products = [];
		cart.amount = 0;
		await cart.save();
		return order;		
	} catch (error) {
		console.log(error);
		return 'Something went wrong';
	}
};

// Retrieve all orders (ADMIN only)
module.exports.getAllOrders = async (data) => {
	if (data.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		});
	} else {
		let message = Promise.resolve('User must be ADMIN to access this!');
		return message.then((value) => {
			return{value};
		});
	}	
};

// Get monthly sales report (ADMIN only)
module.exports.salesReport = async (data) => {
	const date = new Date();
	const lastMonth = new Date(date.setMonth(date.getMonth() - 1));
	const previousMonth = new Date(new Date().setMonth(lastMonth.getMonth() - 1));

	if (data.isAdmin) {
		try {
			const income = await Order.aggregate([
				{ $match: { purchasedOn: { $gte: previousMonth } } },
				{
					$project: {
						month: { $month: "$purchasedOn" },
						sales: "$amount",
					},
				},
				{
					$group: {
						_id: "$month",
						total: { $sum: "$sales" },
					},
				},
			]);
			return income;
		} catch (error) {
			return 'Something went wrong';
		}
	}
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return{value};
	});		
};
