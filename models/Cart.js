// Cart.js

const mongoose = require("mongoose");
const ObjectID = mongoose.Schema.Types.ObjectId

const cartSchema = new mongoose.Schema({
	userId : {
		type : ObjectID,		
		required : true,
		ref: 'User'
	},
	products : [
		{			
			productId : {
				type : ObjectID,
				required : true,
				ref: 'Product'
			},
			name: {
				type: String,
				required: true
			},
			quantity: {
				type: Number,
				required: true,
				min: 1,
				default: 1
			},
			price: {
				type: Number,
				required: true
			},
			subtotal: {
				type: Number,
				required: true
			}
		}
	],
	amount: {
		type: Number,
		required: true		
	},
	addedOn : {
		type : Date, 
		default : new Date()
	}	
});

module.exports = mongoose.model("Cart", cartSchema);