const express = require("express");

const router = express.Router();

const orderController = require("../controllers/orderController");

const auth = require("../auth");


// Route for retrieving order
router.get("/retrieve", auth.verify, (req, res) => {	
	
	const userId = auth.decode(req.headers.authorization).id;		
		
	orderController.getOrder(userId).then(resultFromController => res.send(resultFromController));
});


// Route for user checkout
router.post("/checkout", auth.verify, (req, res) => {	
	
	const userId = auth.decode(req.headers.authorization).id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;		
		
	orderController.checkOut(userId, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving ALL orders (ADMIN only)
router.get("/all", auth.verify, (req, res) => {	

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}		
		
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});

// Route for monthly sales report (ADMIN only)
router.get("/sales", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}		
		
	orderController.salesReport(data).then(resultFromController => res.send(resultFromController));
});





module.exports = router;