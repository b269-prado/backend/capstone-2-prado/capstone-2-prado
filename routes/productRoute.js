// productRoute.js

const express = require("express");

const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");


// Route for creating a new product (ADMIN only)
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving ALL products (ADMIN only)
router.get("/all", (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving ACTIVE products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a SPECIFIC product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product (ADMIN only)
router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});


// Route for archiving a specific product (ADMIN only)
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	const shouldArchive = !req.path.endsWith('/unarchive');

	productController.archiveProduct(data, req.params, shouldArchive).then(resultFromController => res.send(resultFromController));
});




module.exports = router;