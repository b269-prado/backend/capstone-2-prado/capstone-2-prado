const express = require("express");
const router = express.Router();

const cartController = require("../controllers/cartController");

const auth = require("../auth");


// Route for retrieving cart
router.get("/retrieve", auth.verify, (req, res) => {	
	
	const userId = auth.decode(req.headers.authorization).id;		
		
	cartController.getCart(userId).then(resultFromController => res.send(resultFromController));
});


// Route for adding products to cart
router.post("/add", auth.verify, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id;	
	const {productId, quantity} = req.body;
	
	cartController.addToCart(userId, { productId, quantity }).then(resultFromController => res.send(resultFromController));
});


// Route for removing products from cart
router.delete("/:productId/remove", auth.verify, (req, res) => {
	
	const userId = auth.decode(req.headers.authorization).id;	

	cartController.removeFromCart(userId, req.params).then(resultFromController => res.send(resultFromController));
});





module.exports = router;