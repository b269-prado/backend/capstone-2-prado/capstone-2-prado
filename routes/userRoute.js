// userRoute.js

const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
			resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
			resultFromController => res.send(resultFromController));
});


// Route for setting user as an admin (ADMIN only)
router.patch("/:userId/set-admin", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsAdmin(data, req.params).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

module.exports = router;