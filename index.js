// Capstone 2

// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute"); 
const orderRoute = require("./routes/orderRoute"); 
const cartRoute = require("./routes/cartRoute"); 

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/products", productRoute); 
app.use("/orders", orderRoute); 
app.use("/carts", cartRoute); 

// Database connection
mongoose.connect("mongodb+srv://gerprado0718:admin123@zuitt-bootcamp.bntxtqp.mongodb.net/ecommerceAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log(`Now connected to the cloud database.`));

app.listen(process.env.PORT || 4001, () => console.log(`Now connected to port ${process.env.PORT || 4001}`));
